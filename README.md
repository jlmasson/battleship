# Battleship

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 9.1.15 and upgraded to Angular 13.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running linting tests

Run `ng lint` to execute the linting tests

## Data Management

All data is managed in memory through services.

There are two services:

- RecordsService (Game's records)
- SettingsService (Game's settings)

## External Libraries Used

- [SweetAlert2](https://sweetalert2.github.io/)
- [NGX Bootstrap](https://valor-software.com/ngx-bootstrap/)

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
