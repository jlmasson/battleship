import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {BaseLayoutComponent} from './layout/base-layout/base-layout.component';


const routes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    redirectTo: '/battleship/menu',
  },
  {
    path: '',
    component: BaseLayoutComponent,
    children: [
      { path: 'battleship', loadChildren: () => import('./battleship/battleship.module').then(m => m.BattleshipModule)  }
    ]
  },
  {
    path: '**',
    redirectTo: '/battleship/menu'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
