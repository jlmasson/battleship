export const SettingsConfig = {
  boardSize: 10,
  levels: {
    easy: {
      label: 'Fácil',
        turns: 0
    },
    normal: {
      label: 'Intermedio',
        turns: 100
    },
    difficult: {
      label: 'Difícil',
        turns: 50
    }
  }
};
