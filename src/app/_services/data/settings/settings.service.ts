import { Injectable } from '@angular/core';

import { SettingsConfig } from './settings.data';

@Injectable({
  providedIn: 'root'
})
export class SettingsService {

  defaultConfig: any;

  constructor() {
    this.defaultConfig = SettingsConfig;
  }

  updateConfig(config: any) {
    this.defaultConfig = config;
  }
}
