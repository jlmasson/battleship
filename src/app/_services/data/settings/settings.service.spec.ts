import { TestBed } from '@angular/core/testing';

import { SettingsService } from './settings.service';

describe('SettingsService', () => {
  let service: SettingsService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(SettingsService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should update settings', () => {
    const newConfig = {
      boardSize: 15,
      levels: {
        easy: {
          label: 'Fácil',
          turns: 0
        },
        normal: {
          label: 'Intermedio',
          turns: 100
        },
        difficult: {
          label: 'Complicado',
          turns: 50
        }
      }
    };
    service.updateConfig(newConfig);
    expect(service.defaultConfig).toEqual(newConfig);
  });
});
