import { TestBed } from '@angular/core/testing';

import { RecordsService } from './records.service';

describe('RecordsService', () => {
  let service: RecordsService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(RecordsService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should save a record', () => {
    const totalBefore = service.defaultData.length;
    const newRecord = {
      name: 'Test',
      level: 'Libre',
      turns: 50,
      shipsDestroyed: 3,
      won: false
    };
    service.saveRecord(newRecord);
    const totalAfter = service.defaultData.length;
    expect(totalAfter).toEqual(totalBefore + 1);
  });
});
