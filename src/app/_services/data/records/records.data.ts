export const RecordsData = [
  {
    name: 'José Luis',
    level: 'Fácil',
    turns: 50,
    shipsDestroyed: 10,
    won: true
  },
  {
    name: 'José Luis',
    level: 'Difícil',
    turns: 50,
    shipsDestroyed: 3,
    won: false
  }
];
