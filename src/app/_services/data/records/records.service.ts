import { Injectable } from '@angular/core';
import {RecordsData} from './records.data';

@Injectable({
  providedIn: 'root'
})
export class RecordsService {

  defaultData: any[];

  constructor() {
    this.defaultData = RecordsData;
  }

  saveRecord(record: any) {
    this.defaultData.push(record);
  }

}
