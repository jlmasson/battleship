import { Injectable } from '@angular/core';
import {Board, PositionEnum} from '../../_domain';

@Injectable({
  providedIn: 'root'
})
export class BoardService {

  shipsCatalog: any[] = [
    { totalShips: 1, size: 4 },
    { totalShips: 2, size: 3 },
    { totalShips: 3, size: 2 },
    { totalShips: 4, size: 1 },
  ];

  constructor() { }

  public static getRandomInt(minValue, maxValue) {
    return Math.floor(Math.random() * (maxValue - minValue + 1)) + minValue;
  }

  createBoard(rows: number, cols: number): Board {
    const board = new Board(rows, cols);
    this.putRandomShips(board);
    return board;
  }

  putRandomShips(board: Board) {
    let id = 1;
    for (const catalogItem of this.shipsCatalog) {
      for (let i = 0; i < catalogItem.totalShips; i++) {
        // Validate Position
        let rowIndex; let colIndex;
        const axis = BoardService.getRandomInt(PositionEnum.VERTICAL, PositionEnum.HORIZONTAL);
        do {
          rowIndex = BoardService.getRandomInt(0, board.rows - 1);
          colIndex = BoardService.getRandomInt(0, board.cols - 1);
        } while (!board.isAvailable(rowIndex, colIndex, catalogItem.size, axis));
        board.putShip(id, rowIndex, colIndex, catalogItem.size, axis);
        id++;
      }
    }
  }

}
