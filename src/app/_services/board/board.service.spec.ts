import { TestBed } from '@angular/core/testing';

import { BoardService } from './board.service';

describe('BoardService', () => {
  let service: BoardService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(BoardService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should create the board', () => {
    const board = service.createBoard(5, 5);
    expect(board.sea).toBeTruthy();
  });

  it('should create the board with 10 ships', () => {
    const board = service.createBoard(10, 10);
    expect(Object.keys(board.ships).length).toEqual(10);
  });
});
