import {Component, OnInit} from '@angular/core';
import {BoardService} from './_services/board/board.service';
import {Board, BoardCell, DamageEnum} from './_domain';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'battleship';
  turns: number;
  board: Board;

  constructor(private boardService: BoardService) {
  }
}
