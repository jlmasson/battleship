export enum AlertTypeEnum {
  SUCCESS,
  ERROR,
  CONFIRM,
  GENERAL,
  WARNING,
  LOADING
}
