import Swal, {SweetAlertOptions} from 'sweetalert2';
import {AlertTypeEnum} from '../enums';

const Toast = Swal.mixin({
  customClass: {
    confirmButton: 'btn btn-sm btn-primary',
    cancelButton: 'btn btn-sm btn-danger'
  },
  cancelButtonText: 'Cancelar',
  buttonsStyling: false,
  toast: true,
  position: 'top-end',
  showConfirmButton: false,
  timer: 3000,
  timerProgressBar: true,
  didOpen: (toast) => {
    toast.addEventListener('mouseenter', Swal.stopTimer);
    toast.addEventListener('mouseleave', Swal.resumeTimer);
  }
});

export function generateToast(alertType: AlertTypeEnum, config: Partial<SweetAlertOptions>) {
  switch (alertType) {
    case AlertTypeEnum.SUCCESS:
      return generateSuccessToast(config);
    case AlertTypeEnum.ERROR:
      return generateErrorToast(config);
    case AlertTypeEnum.CONFIRM:
      return generateConfirmToast(config);
    case AlertTypeEnum.GENERAL:
      return generateGeneralToast(config);
    case AlertTypeEnum.WARNING:
      return generateWarningToast(config);
    default:
      return generateGeneralToast(config);
  }
}

const generateSuccessToast = (config: Partial<SweetAlertOptions>) => {
  const options: SweetAlertOptions = {
    ...config,
    icon: 'success',
    title: config.title ? config.title : 'Éxito',
  };
  return Toast.fire(options);
};

const generateErrorToast = (config: Partial<SweetAlertOptions>) => {
  const options: SweetAlertOptions = {
    ...config,
    icon: 'error',
    title: config.title ? config.title : 'Error',
  };
  return Toast.fire(options);
};

const generateWarningToast = (config: Partial<SweetAlertOptions>) => {
  const options: SweetAlertOptions = {
    ...config,
    icon: 'warning',
    title: config.title ? config.title : 'Alerta'
  };
  return Toast.fire(options);
};

const generateGeneralToast = (config: Partial<SweetAlertOptions>) => {
  const options: SweetAlertOptions = {
    ...config,
    icon: 'info',
  };
  return Toast.fire(options);
};

const generateConfirmToast = (config: Partial<SweetAlertOptions>) => {
  const options: SweetAlertOptions = {
    ...config,
    icon: 'warning',
    showConfirmButton: true,
    showCancelButton: true
  };
  return Toast.fire(options);
};
