import Swal, {SweetAlertOptions} from 'sweetalert2';
import {AlertTypeEnum} from '../enums';

const swalWithBootstrapButtons = Swal.mixin({
  customClass: {
    confirmButton: 'btn btn-sm btn-primary',
    cancelButton: 'btn btn-sm btn-danger'
  },
  cancelButtonText: 'Cancelar',
  buttonsStyling: false
});

export function generateAlert(alertType: AlertTypeEnum, config: Partial<SweetAlertOptions>) {
  switch (alertType) {
    case AlertTypeEnum.SUCCESS:
      return generateSuccessAlert(config);
    case AlertTypeEnum.ERROR:
      return generateErrorAlert(config);
    case AlertTypeEnum.CONFIRM:
      return generateConfirmAlert(config);
    case AlertTypeEnum.GENERAL:
      return generateGeneralAlert(config);
    case AlertTypeEnum.WARNING:
      return generateWarningAlert(config);
    default:
      return generateGeneralAlert(config);
  }
}

const generateSuccessAlert = (config: Partial<SweetAlertOptions>) => {
  const options: SweetAlertOptions = {
    ...config,
    icon: 'success',
    title: config.title ? config.title : 'Éxito',
  };
  return swalWithBootstrapButtons.fire(options);
};

const generateErrorAlert = (config: Partial<SweetAlertOptions>) => {
  const options: SweetAlertOptions = {
    ...config,
    icon: 'error',
    title: config.title ? config.title : 'Error',
  };
  return swalWithBootstrapButtons.fire(options);
};

const generateWarningAlert = (config: Partial<SweetAlertOptions>) => {
  const options: SweetAlertOptions = {
    ...config,
    icon: 'warning',
    title: config.title ? config.title : 'Alerta'
  };
  return swalWithBootstrapButtons.fire(options);
};

const generateGeneralAlert = (config: Partial<SweetAlertOptions>) => {
  const options: SweetAlertOptions = {
    ...config,
    icon: 'info',
  };
  return swalWithBootstrapButtons.fire(options);
};

const generateConfirmAlert = (config: Partial<SweetAlertOptions>) => {
  const options: SweetAlertOptions = {
    ...config,
    icon: 'warning',
    showConfirmButton: true,
    showCancelButton: true
  };
  return swalWithBootstrapButtons.fire(options);
};
