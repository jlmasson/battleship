import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { NewGameComponent } from './new-game/new-game.component';
import { MenuComponent } from './menu/menu.component';
import {SettingsComponent} from './settings/settings.component';
import {RecordsComponent} from './records/records.component';

const routes: Routes = [
  { path: '', pathMatch: 'full', redirectTo: 'menu' },
  { path: 'menu', component: MenuComponent },
  { path: 'new-game', component: NewGameComponent },
  { path: 'settings', component: SettingsComponent },
  { path: 'records', component: RecordsComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BattleshipRoutingModule { }
