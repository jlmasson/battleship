import { Component, OnInit } from '@angular/core';
import {RecordsService} from '../../_services/data/records/records.service';

@Component({
  selector: 'app-records',
  templateUrl: './records.component.html',
  styleUrls: ['./records.component.scss']
})
export class RecordsComponent implements OnInit {

  records: any;

  constructor(
    private recordsService: RecordsService
  ) { }

  ngOnInit(): void {
    this.records = this.recordsService.defaultData;
  }

}
