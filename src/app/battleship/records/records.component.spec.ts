import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RecordsComponent } from './records.component';
import { RecordsService } from '../../_services/data/records/records.service';

describe('RecordsComponent', () => {
  let component: RecordsComponent;
  let fixture: ComponentFixture<RecordsComponent>;
  let service: RecordsService;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RecordsComponent ]
    })
    .compileComponents();
    service = TestBed.inject(RecordsService);
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RecordsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should render records', () => {
    const compiled = fixture.nativeElement;
    expect(compiled.querySelectorAll('table tbody tr').length).toEqual(service.defaultData.length);
  });

  it('should render new records', () => {
    const newRecord = {
      name: 'José Luis',
      level: 'Difícil',
      turns: 50,
      shipsDestroyed: 3,
      won: false
    };
    service.saveRecord(newRecord);
    const totalRecords = service.defaultData.length;
    fixture.detectChanges();
    const compiled = fixture.nativeElement;
    expect(compiled.querySelectorAll('table tbody tr').length).toEqual(totalRecords);
  });

});
