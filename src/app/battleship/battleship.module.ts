import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { BattleshipRoutingModule } from './battleship-routing.module';
import { NewGameComponent } from './new-game/new-game.component';
import { MenuComponent } from './menu/menu.component';
import { BoardComponent } from './new-game/board/board.component';
import { SettingsComponent } from './settings/settings.component';
import { RecordsComponent } from './records/records.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { LevelGroupComponent } from './settings/forms/level-group/level-group.component';
import { InfoMessageComponent } from './settings/forms/info-message/info-message.component';


@NgModule({
  declarations: [
    NewGameComponent,
    MenuComponent,
    BoardComponent,
    SettingsComponent,
    RecordsComponent,
    LevelGroupComponent,
    InfoMessageComponent,
  ],
  imports: [
    CommonModule,
    BattleshipRoutingModule,
    ReactiveFormsModule,
    FormsModule
  ]
})
export class BattleshipModule { }
