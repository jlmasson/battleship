import {Component, OnInit} from '@angular/core';
import {SettingsService} from '../../_services/data/settings/settings.service';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {AlertTypeEnum, generateToast} from '../../_utils';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
  selector: 'app-settings',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.scss']
})
export class SettingsComponent implements OnInit {

  form: FormGroup;

  constructor(
    private fb: FormBuilder,
    private router: Router,
    private route: ActivatedRoute,
    private settingsService: SettingsService,
  ) { }

  ngOnInit(): void {
    this.buildForm();
    this.form.patchValue(this.settingsService.defaultConfig);
  }

  onSubmit() {
    if (this.form.valid) {
      const data = Object.assign({}, this.form.value);
      generateToast(AlertTypeEnum.GENERAL, { title: 'Actualizando' }).then(r => r);
      this.settingsService.updateConfig(data);
      generateToast(AlertTypeEnum.SUCCESS, { title: 'Guardado Exitosamente' }).then(r => r);
      this.router.navigate(['../'], { relativeTo: this.route }).then(r => r);
    }
  }

  private buildForm() {
    this.form = this.fb.group({
      boardSize: ['', [Validators.required, Validators.min(10), Validators.max(20)]],
      levels: this.fb.group({
        easy: this.buildLevelGroup(),
        normal: this.buildLevelGroup(),
        difficult: this.buildLevelGroup()
      })
    });
  }

  private buildLevelGroup(): FormGroup {
    return this.fb.group({
      label: ['', [Validators.required]],
      turns: ['', [Validators.required, Validators.min(0)]]
    });
  }
}
