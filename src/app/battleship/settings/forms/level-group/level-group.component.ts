import {Component, Input, OnInit} from '@angular/core';
import {FormGroup, FormGroupDirective} from '@angular/forms';

@Component({
  selector: 'app-level-group',
  templateUrl: './level-group.component.html',
  styleUrls: ['./level-group.component.scss']
})
export class LevelGroupComponent implements OnInit {

  @Input() groupName: string;

  form: FormGroup;

  constructor(
    private rootFormGroup: FormGroupDirective
  ) {}

  ngOnInit(): void {
    this.form = this.rootFormGroup.control.get(this.groupName) as FormGroup;
  }

}
