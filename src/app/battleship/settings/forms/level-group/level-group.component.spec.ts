import { ComponentFixture, TestBed } from '@angular/core/testing';
import {FormBuilder, FormGroupDirective, FormsModule, ReactiveFormsModule} from '@angular/forms';

import { LevelGroupComponent } from './level-group.component';

describe('LevelGroupComponent', () => {
  let component: LevelGroupComponent;
  let fixture: ComponentFixture<LevelGroupComponent>;

  beforeEach(async () => {
    const fb = new FormBuilder();
    const formGroupDirective = new FormGroupDirective([], []);
    formGroupDirective.form = fb.group({
      label: fb.control(null),
      turns: fb.control(null)
    });

    await TestBed.configureTestingModule({
      imports: [ FormsModule, ReactiveFormsModule ],
      declarations: [ LevelGroupComponent ],
      providers: [
        FormGroupDirective,
        FormBuilder,
        {provide: FormGroupDirective, useValue: formGroupDirective}
      ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LevelGroupComponent);
    component = fixture.componentInstance;
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
