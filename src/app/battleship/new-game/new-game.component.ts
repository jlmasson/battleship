import {Component, OnInit} from '@angular/core';
import {SettingsService} from '../../_services/data/settings/settings.service';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
  selector: 'app-new-game',
  templateUrl: './new-game.component.html',
  styleUrls: ['./new-game.component.scss']
})
export class NewGameComponent implements OnInit {

  isGameStarted = false;
  levelsConfig: any;
  selectedLevel: any;
  playerName = '';
  isFree: boolean;
  freeTurns: number;
  isAvailableToFill = false;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private settingsService: SettingsService,
  ) { }

  ngOnInit(): void {
    this.levelsConfig = this.settingsService.defaultConfig.levels;
  }

  startGame() {
    this.isGameStarted = false;
    if (this.isFree) {
      this.selectedLevel.turns = this.freeTurns;
    }
    this.isGameStarted = true;
  }

  fillData(levelConfig: any, isFree = false) {
    this.isAvailableToFill = true;
    this.isFree = isFree;
    this.selectedLevel = Object.assign({}, levelConfig);
  }

}
