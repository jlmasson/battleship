import {ComponentFixture, fakeAsync, TestBed, tick} from '@angular/core/testing';

import { BoardComponent } from './board.component';
import {RouterTestingModule} from '@angular/router/testing';
import {RecordsService} from '../../../_services/data/records/records.service';
import {BoardService} from '../../../_services/board/board.service';

describe('BoardComponent', () => {
  let component: BoardComponent;
  let fixture: ComponentFixture<BoardComponent>;
  let recordsService: RecordsService;
  let boardService: BoardService;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [ RouterTestingModule ],
      declarations: [ BoardComponent ]
    })
    .compileComponents();
    recordsService = TestBed.inject(RecordsService);
    boardService = TestBed.inject(BoardService);
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BoardComponent);
    component = fixture.componentInstance;
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it ('should render initial data', () => {
    const expectedPlayerName = 'Test Player';
    const expectedLevelConfig: { label: string, turns: number } = {
      label: 'Normal',
      turns: 50
    };
    component.playerName = expectedPlayerName;
    component.levelConfig = expectedLevelConfig;
    fixture.detectChanges();

    const compiled = fixture.nativeElement;
    const resultValue = {
      level: compiled.querySelector('.level').textContent,
      turns: +compiled.querySelector('.turns').textContent,
      playerName: compiled.querySelector('.player-name').textContent,
      cells: compiled.querySelectorAll('.board-cell').length,
      rowsLabels: +compiled.querySelectorAll('.row-label-cell').length,
      colsLabels: +compiled.querySelectorAll('.col-label-cell').length,
    };

    const expectedValue = {
      level: expectedLevelConfig.label,
      turns: +expectedLevelConfig.turns,
      playerName: expectedPlayerName,
      cells: component.board.rows * component.board.cols,
      rowsLabels: +component.board.rowLabels.length,
      colsLabels: +component.board.colLabels.length,
    };

    expect(resultValue).toEqual(expectedValue);
  });

  it('should render shot', () => {
    const expectedPlayerName = 'Test Player';
    const expectedLevelConfig: { label: string, turns: number } = {
      label: 'Normal',
      turns: 50
    };
    component.playerName = expectedPlayerName;
    component.levelConfig = expectedLevelConfig;
    fixture.detectChanges();

    // Random Board Cell
    const row = BoardService.getRandomInt(0, component.board.rows - 1);
    const col = BoardService.getRandomInt(0, component.board.cols - 1);

    const boardCell = component.board.sea[row][col];
    component.fireTorpedo(boardCell);
    expect(boardCell.isUsed).toBeTruthy();
  });
});
