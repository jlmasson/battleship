import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {Board, BoardCell, DamageEnum} from '../../../_domain';
import {AlertTypeEnum, generateAlert, generateToast} from '../../../_utils';
import {BoardService} from '../../../_services/board/board.service';
import {SettingsService} from '../../../_services/data/settings/settings.service';
import {RecordsService} from '../../../_services/data/records/records.service';

@Component({
  selector: 'app-board',
  templateUrl: './board.component.html',
  styleUrls: ['./board.component.scss']
})
export class BoardComponent implements OnInit {

  @Input() playerName: string;
  @Input() levelConfig: { label: string, turns: number };
  @Output() gameFinished = new EventEmitter<boolean>();

  board: Board;
  turnsGame: number;
  turnsPlayed = 0;
  isResultSaved = false;
  isFinished = false;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private settingsService: SettingsService,
    private recordsService: RecordsService,
    private boardService: BoardService,
  ) {}

  ngOnInit(): void {
    this.startGame();
  }

  get isLost() {
    if (this.levelConfig.turns === 0) {
      return false;
    }
    return this.turnsGame === 0 && !this.board?.isBoardEmpty();
  }

  restartGame() {
    this.startGame();
  }

  async returnMenu() {
    if (!this.isFinished) {
      const r = await generateAlert(AlertTypeEnum.CONFIRM, {
        title: '¿Desea volver al menú principal? Este juego aún no ha terminado'
      });
      if (r.isConfirmed) {
        this.navigateToMenu();
      }
    } else {
      this.navigateToMenu();
    }
  }

  fireTorpedo(cell: BoardCell) {
    if (this.board.isBoardEmpty()) {
      generateToast(AlertTypeEnum.SUCCESS, { title: 'Has destruido todos los barcos' }).then(r => r);
      this.finishGame(true);
      return;
    }
    // Si los turnos configurados en el nivel es 0, se entiende que es infinito
    if (this.turnsGame > 0 || this.levelConfig.turns === 0) {
      if (cell.isUsed) {
        generateToast(AlertTypeEnum.WARNING, {title: '¡Ya has golpeado esto!'}).then(r => r);
        return;
      }

      if (cell.value === 0) {
        generateToast(AlertTypeEnum.ERROR, {title: '¡Has fallado!'}).then(r => r);
        cell.status = '❌';
      } else {
        const damage = this.board.damageShip(cell.value);
        if (damage === DamageEnum.TOTAL) {
          generateToast(AlertTypeEnum.SUCCESS, {title: '¡Has hundido un barco!'}).then(r => r);
        } else {
          generateToast(AlertTypeEnum.GENERAL, {title: '¡Has acertado!'}).then(r => r);
        }
        cell.status = cell.value + ' - 🚢';
        cell.value = -1;
      }
      cell.isUsed = true;
      this.turnsPlayed++;
      // Solo se resta cuando los turnos configurados en el nivel sean diferente de 0
      if (this.levelConfig.turns !== 0) {
        this.turnsGame--;
        if (this.turnsGame === 0) {
          if (this.board.isBoardEmpty()) {
            this.finishGame(true);
          } else {
            this.finishGame(false);
          }
        }
      } else {
        if (this.board.isBoardEmpty()) {
          this.finishGame(true);
        }
      }
    } else {
      generateToast(AlertTypeEnum.ERROR, { title: 'Tus turnos ya se han acabado' }).then(r => r);
    }
  }

  private startGame() {
    this.turnsGame = this.levelConfig.turns;
    this.turnsPlayed = 0;
    this.isResultSaved = false;
    this.isFinished = false;
    this.board = this.boardService.createBoard(this.settingsService.defaultConfig.boardSize, this.settingsService.defaultConfig.boardSize);
  }

  private finishGame(won: boolean) {
    if (!this.isResultSaved) {
      this.recordsService.saveRecord({
        name: this.playerName,
        level: this.levelConfig.label,
        turns: this.turnsPlayed,
        shipsDestroyed: this.board.shipsDestroyed,
        won
      });
      this.isFinished = true;
      this.isResultSaved = true;
      this.gameFinished.emit(this.isFinished);
    }
  }

  private navigateToMenu() {
    this.router.navigate(['../'], { relativeTo: this.route }).then(r => r);
  }
}
