export class BoardCell {
  #isUsed: boolean;
  #value: number;
  #status: string;

  constructor(isUsed?: boolean, value?: number, status?: string) {
    this.#isUsed = isUsed ? isUsed : false;
    this.#value = value ? value : 0;
    this.#status = status ? status : '❓';
  }

  get isUsed(): boolean {
    return this.#isUsed;
  }

  set isUsed(value: boolean) {
    this.#isUsed = value;
  }

  get value(): number {
    return this.#value;
  }

  set value(value: number) {
    this.#value = value;
  }

  get status(): string {
    return this.#status;
  }

  set status(value: string) {
    this.#status = value;
  }
}
