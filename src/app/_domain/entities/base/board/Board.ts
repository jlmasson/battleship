import {BoardCell} from './BoardCell';
import {DamageEnum, PositionEnum} from '../../enums';
import {Ship} from '../ship';

export class Board {
  #rows: number;
  #cols: number;
  #rowLabels: string[];
  #colLabels: number[];
  #ships: {[id: number]: Ship};
  #shipsDestroyed: number;
  #totalShips: number;
  #sea: BoardCell[][];

  constructor(rows: number, cols: number) {
    this.#rows = rows;
    this.#cols = cols;
    this.#ships = {};
    this.#shipsDestroyed = 0;
    this.#totalShips = 0;
    this.setupSea();
    this.setupLabels();
  }

  isAvailable(row: number, col: number, size: number, axis: PositionEnum): boolean {
    if (this.#sea[row][col].value !== 0) {
      return false;
    }
    switch (axis) {
      case PositionEnum.HORIZONTAL:
        if (col + size >= this.#rows) {
          return false;
        }
        for (let j = col; j < col + size; j++) {
          if (this.#sea[row][j].value !== 0) {
            return false;
          }
        }
        break;
      case PositionEnum.VERTICAL:
        if (row + size >= this.#cols) {
          return false;
        }
        for (let i = row; i < row + size; i++) {
          if (this.#sea[i][col].value !== 0) {
            return false;
          }
        }
        break;
    }
    return true;
  }

  isBoardEmpty() {
    return this.#shipsDestroyed === this.#totalShips;
  }

  damageShip(id: number): DamageEnum {
    this.#ships[id].damage();
    if (this.#ships[id].isDestroyed) {
      this.shipsDestroyed++;
    }
    return this.#ships[id].isDestroyed ? DamageEnum.TOTAL : DamageEnum.PARTIAL;
  }

  putShip(id: number, row: number, col: number, size: number, axis: PositionEnum) {
    const ship = new Ship(id, size, axis);
    switch (axis) {
      case PositionEnum.HORIZONTAL:
        for (let j = col; j < col + size; j++) {
          this.#sea[row][j].value = ship.id;
        }
        break;
      case PositionEnum.VERTICAL:
        for (let i = row; i < row + size; i++) {
          this.#sea[i][col].value = ship.id;
        }
        break;
    }
    this.#ships[id] = ship;
    this.#totalShips++;
  }

  private setupSea() {
    this.#sea = [];
    for (let i = 0; i < this.#rows; i++) {
      this.#sea[i] = [];
      for (let j = 0; j < this.#cols; j++) {
        this.#sea[i][j] = new BoardCell();
      }
    }
  }

  private setupLabels() {
    this.setupRowLabels();
    this.setupColLabels();
  }

  private setupRowLabels() {
    const asciiInitial = 65;
    this.#rowLabels = [];
    for (let i = 0; i < this.#rows; i++) {
      this.#rowLabels.push(String.fromCharCode(asciiInitial + i));
    }
  }

  private setupColLabels() {
    this.#colLabels = [];
    for (let i = 1; i <= this.#cols; i++) {
      this.#colLabels.push(i);
    }
  }

  get rows(): number {
    return this.#rows;
  }

  set rows(value: number) {
    this.#rows = value;
  }

  get cols(): number {
    return this.#cols;
  }

  set cols(value: number) {
    this.#cols = value;
  }

  get rowLabels(): string[] {
    return this.#rowLabels;
  }

  set rowLabels(value: string[]) {
    this.#rowLabels = value;
  }

  get colLabels(): number[] {
    return this.#colLabels;
  }

  set colLabels(value: number[]) {
    this.#colLabels = value;
  }

  get ships(): { [p: number]: Ship } {
    return this.#ships;
  }

  set ships(value: { [p: number]: Ship }) {
    this.#ships = value;
  }

  get shipsDestroyed(): number {
    return this.#shipsDestroyed;
  }

  set shipsDestroyed(value: number) {
    this.#shipsDestroyed = value;
  }

  get totalShips(): number {
    return this.#totalShips;
  }

  set totalShips(value: number) {
    this.#totalShips = value;
  }

  get sea(): BoardCell[][] {
    return this.#sea;
  }

  set sea(value: BoardCell[][]) {
    this.#sea = value;
  }
}
