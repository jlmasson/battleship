import {PositionEnum} from '../../enums';

export class Ship {
  #id: number;
  #size: number;
  #remaining: number;
  #position: PositionEnum;
  #isDestroyed: boolean;

  constructor(id: number, size: number, position: PositionEnum) {
    this.#id = id;
    this.#size = size;
    this.#remaining = this.#size;
    this.#position = position;
    this.#isDestroyed = false;
  }

  damage() {
    this.#remaining--;
    this.#isDestroyed = this.#remaining === 0;
  }

  get id(): number {
    return this.#id;
  }

  set id(value: number) {
    this.#id = value;
  }

  get size(): number {
    return this.#size;
  }

  set size(value: number) {
    this.#size = value;
  }

  get remaining(): number {
    return this.#remaining;
  }

  set remaining(value: number) {
    this.#remaining = value;
  }

  get position(): PositionEnum {
    return this.#position;
  }

  set position(value: PositionEnum) {
    this.#position = value;
  }

  get isDestroyed(): boolean {
    return this.#isDestroyed;
  }

  set isDestroyed(value: boolean) {
    this.#isDestroyed = value;
  }
}
