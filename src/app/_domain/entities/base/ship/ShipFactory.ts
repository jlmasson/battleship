import {PositionEnum} from '../../enums';
import {Ship} from './Ship';

export class ShipFactory {
  createShipInstance(id: number, size: number, axis: PositionEnum): Ship {
    return new Ship(id, size, axis);
  }
}
